<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Currency\Ctrl;

use Capwelton\App\Currency\Set\CurrencySet;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('Currency');


/**
 * This controller manages actions that can be performed on currencies.
 *
 * @method \Func_App App()
 */
class CurrencyController extends \app_ComponentCtrlRecord
{
    
    
    protected function toolbar(\widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $toolbar = parent::toolbar($tableView);
        $toolbar->addItem(
            $W->Link(
                $App->translate('Add currency'),
                $this->proxy()->edit()
            )->setIcon(\Func_Icons::ACTIONS_LIST_ADD)->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        $toolbar->addItem(
            $W->Link(
                $App->translate('Set default currency'),
                $this->proxy()->editDefaultCurrency()
            )->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        return $toolbar;
    }
    
    public function editDefaultCurrency()
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();
        $page = $Ui->Page();
        $page->setTitle($App->translate('Set default currency'));
        
        $editor = new \app_Editor($App, null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $editor->setName('data');
        $editor->isAjax = true;
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setSaveAction($this->proxy()->saveDefaultCurrency(), $App->translate('Save'));
        
        $set = $App->CurrencySet();
        $records = $set->select()->orderAsc($set->name_fr);
        $options = array();
        foreach ($records as $record){
            $options[$record->id] = $record->name_fr();
        }
        
        
        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Default currency'),
                $select = $W->Select()->setOptions($options),
                'currency'
            )
        );
        
        $currentDefault = $set->get($set->isDefault->is(true));
        if($currentDefault){
            $select->setValue($currentDefault->id);
        }
        
        $page->addItem($editor);
        
        return $page;
    }
    
    public function saveDefaultCurrency($data = null)
    {
        $this->requireSaveMethod();
        
        $App = $this->App();
        $set = $App->CurrencySet();
        
        $newDefault = $set->get($set->id->is($data['currency']));
        if($newDefault){
            $newDefault->setAsDefaultCurrency();
            $this->addMessage($App->translate('The default currency has been changed'));
        }
        else{
            $this->addError($App->translate('The selected currency was not found'));
        }
        
        $this->addReloadSelector('.depends-currency');
        
        return true;
    }
}