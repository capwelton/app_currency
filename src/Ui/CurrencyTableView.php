<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Currency\Ui;

use Capwelton\App\Currency\Ctrl\CurrencyController;
use Capwelton\App\Currency\Set\Currency;
use Capwelton\App\Currency\Set\CurrencySet;

class CurrencyTableView extends \app_TableModelView
{
    protected $currencyCtrl;
    protected $widgets;
    
    /**
     * @param \Func_App $App
     * @param string $id
     */
    public function __construct(\Func_App $App = null, $id = null)
    {
        $this->currencyCtrl = $App->Controller()->Currency();
        $this->widgets = bab_Widgets();
        parent::__construct($App, $id);
        $App = $this->App();
        $this->addClass(\Func_Icons::ICON_LEFT_16);
        $this->addClass('depends-currency');
    }
    
    
    /**
     * @param \app_CtrlRecord $recordController
     * @return self
     */
    public function setRecordController(CurrencyController $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }
    
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(\ORM_RecordSet $set)
    {
        /* @var $set CurrencySet */
        $App = $this->App();
        $this->addColumn(app_TableModelViewColumn($set->name_fr));
        $this->addColumn(app_TableModelViewColumn($set->code));
        $this->addColumn(app_TableModelViewColumn($set->symbol));
        $this->addColumn(app_TableModelViewColumn('_actions_', ' ')->setSortable(false)->setExportable(false)->addClass('widget-column-thin'));
    }    
    
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::computeCellContent()
     */
    protected function computeCellContent(Currency $record, $fieldPath)
    {
        $W = $this->widgets;
        $App = $this->App();        
        
        switch ($fieldPath) {
            case '_actions_':
                $content = $W->VBoxItems();
                $content->setVerticalSpacing(1,'em');
                
                $content->addItem(
                    $W->Link(
                        '',
                        $this->getRecordControllerProxy()->confirmDelete($record->id)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE)
                );
                break;
            case 'name_fr':
                $content = $W->VBoxItems();
                $content->setVerticalSpacing(1,'em');
                
                $content->addItem(
                    $W->Link(
                        $record->name_fr,
                        $this->getRecordControllerProxy()->edit($record->id)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                );
                break;
            default:
                $content = parent::computeCellContent($record, $fieldPath);
                break;
        }
        
        return $content;
    }
    
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param array       $filter
     * @return \ORM_Criteria
     */
    public function getFilterCriteria($filter = null)
    {
        $recordSet = $this->getRecordSet();
        
        $conditions = array(
            $recordSet->isReadable()
        );
        
        $conditions[] = parent::getFilterCriteria($filter);
        
        if (isset($filter['search']) && !empty($filter['search'])) {
            $mixedConditions = array(
                $recordSet->description->contains($filter['search']),
                $recordSet->accountName->contains($filter['search'])
            );
            $conditions[] = $recordSet->any($mixedConditions);
        }
        
        
        return $recordSet->all($conditions);
    }
}