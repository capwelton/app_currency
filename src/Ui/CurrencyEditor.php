<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Currency\Ui;

use Capwelton\App\Currency\Set\Currency;

/**
 * @return CurrencyEditor
 */
class CurrencyEditor extends \app_Editor
{
    protected $record;
    
    /**
     * @param \Func_App App
     * @param Currency $record
     * @param int $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, Currency $record = null, $id = null, \Widget_Layout $layout = null)
    {
        $this->record = $record;
        parent::__construct($App, $id, $layout);
        $this->addFields();
    }

    protected function addFields()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $this->addItem(
            $W->VBoxItems(
                $W->FlexItems(
                    $this->code(),
                    $this->nameEn(),
                    $this->nameFr(),
                    $this->symbol()
                )->setGrowable(),
                $this->rate(),
                $this->id()
            )->setVerticalSpacing(1, 'em')
        );
        
    }
    
    protected function id(){
        return $this->widgets->Hidden(null,"id",null);
    }
    
    protected function code()
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->App()->translate('ISO 4217 code'),
            $W->LineEdit()->addClass('app-fullwidth')->setSize(50),
            'code'
        );
    }    
    
    protected function nameEn()
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->App()->translate('Name (english)'),
            $W->LineEdit()->addClass('app-fullwidth')->setSize(50),
            'name_en'
        );
    }    
    
    protected function nameFr()
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->App()->translate('Name (french)'),
            $W->LineEdit()->addClass('app-fullwidth')->setSize(50),
            'name_fr'
        );
    }    
    
    protected function symbol()
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->App()->translate('Symbol'),
            $W->LineEdit()->addClass('app-fullwidth')->setSize(50),
            'symbol'
        );
    }    
    
    protected function rate()
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->App()->translate('Conversion rate to EUR'),
            $W->NumberEdit()->setStep('0.01')->addClass('app-fullwidth')->setSize(50),
            'rate'
        );
    }
    
    public function setValues($record, $namePathBase = array())
    {
        if ($record instanceof Currency) {
            $recordValues = $record->getValues();
            parent::setValues($recordValues, $namePathBase);
        } else {
            parent::setValues($record, $namePathBase);
        }
    }
}