��          �      L      �     �     �  
   �     �     �     
  
        #     2     @     C     H     ]  %   d  #   �     �     �     �  &  �     �     �                '     :     H     X     f     v     z     �     �  *   �  /   �                                                                                  
             	                    Add currency Conversion rate to EUR Currencies Currency Default currency ISO 4217 code Is default Name (english) Name (french) No Save Set default currency Symbol The default currency has been changed The selected currency was not found Yes _dollar_ _euro_ Project-Id-Version: appcurrency
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-03-28 10:28+0200
Last-Translator: SI4YOU <contact@si-4you.com>
Language-Team: SI4YOU<contact@si-4you.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: libapp_translate;libapp_translate:1,2;translate:1,2;translate;translatable;translatable:1,2
X-Generator: Poedit 3.0.1
X-Poedit-SearchPath-0: .
 Ajouter une devise Taux de conversion vers EUR Devises Devise Devise par défaut Code ISO 4217 Est par défaut Nom (anglais) Nom (français) Non Enregistrer Définir la devise par défaut Symbole La devise par défaut a été enregistrée La devise sélectionnée n'a pas été trouvée Oui $ € 