<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Currency\Set;

include_once 'base.php';

/**
 * @property \ORM_StringField   $code
 * @property \ORM_StringField   $name_en
 * @property \ORM_StringField   $name_fr
 * @property \ORM_StringField   $symbol
 * @property \ORM_DecimalField  $toEuroRate
 * @property \ORM_BoolField     $isDefault
 *
 * @method \Func_App App()
 */
class CurrencySet extends \app_TraceableRecordSet
{
    /**
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $App = $this->App();
        
        $this->setTableName($App->classPrefix.'Currency');
        $this->setDescription($App->translatable('Currency'));
        
        $this->addFields(
            ORM_StringField('code', 3)->setDescription($App->translate('ISO 4217 code')),
            ORM_StringField('name_en', 255)->setDescription($App->translate('Name (english)')),
            ORM_StringField('name_fr', 255)->setDescription($App->translate('Name (french)')),
            ORM_StringField('symbol', 3)->setDescription($App->translate('Symbol')),
            ORM_DecimalField('toEuroRate', 6)->setDescription($App->translate('Conversion rate to EUR')),
            ORM_BoolField('isDefault')->setOutputOptions($App->translate('No'), $App->translate('Yes'))->setDescription($App->translate('Is default'))
            );
    }
    
    
    public function onUpdate(){
        $App = $this->App();
        $set = $App->CurrencySet();
        $currencies = $set->select();
        $component = $App->getComponentByName('CURRENCY');
        if($currencies->count() == 0){
            $currency = $set->newRecord();
            $currency->code = 'EUR';
            $currency->name_fr = 'Euro';
            $currency->name_en = 'Euro';
            $currency->symbol = $component->translate('_euro_');
            $currency->isDefault = true;
            $currency->save();
            
            $currency = $set->newRecord();
            $currency->code = 'USD';
            $currency->name_fr = 'Dollar US';
            $currency->name_en = 'US Dollar';
            $currency->symbol = $component->translate('_dollar_');
            $currency->save();
        }
    }
    /**
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }
    
    
    /**
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }
    
    /**
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
    
    
    /**
     * {@inheritDoc}
     * @see \app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return $this->all();
    }/**
    *
    * {@inheritdoc}
    * @see \app_TraceableRecordSet::save()
    */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new CurrencyBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new CurrencyAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
}

class CurrencyBeforeSaveEvent extends \RecordAfterSaveEvent
{
    
}

class CurrencyAfterSaveEvent extends \RecordBeforeSaveEvent
{
    
}