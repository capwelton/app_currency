<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Currency\Set;

include_once 'base.php';


/**
 * Currency object
 *
 * @property string $code
 * @property string $name_en
 * @property string $name_fr
 * @property string $symbol
 * @property float  $toEuroRate
 * @property bool   $isDefault
 *
 * @method \Func_App         App()
 */
class Currency extends \app_TraceableRecord
{    
    public function setAsDefaultCurrency()
    {
        $set = $this->App()->CurrencySet();
        $otherRecords = $set->select($set->id->isNot($this->id));
        foreach ($otherRecords as $otherRecord){
            $otherRecord->isDefault = false;
            $otherRecord->save();
        }
        $this->isDefault = true;
        $this->save();
    }
}